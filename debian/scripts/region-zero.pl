#!/usr/bin/perl -W
#
# Script to make a given DVD region free ("region 0"), which will get
# rid of some headaches of DVD users.
#
# Assumptions: The DVD has to be ripped already and it must be run from
# the DVD's VIDEO_TS directory. I would be glad if the idea were ported
# to tools like dvdbackup.
#
# Written by Rogério Brito on 2009-02-01, based on xvi's excellent text
# at: http://xvi.rpc1.org/Patching%20DVD%20firmware.pdf
#

sub clean_file {
    open(F, "+<$_[0]") or die("Couldn't open $_[0]: $!");
    seek(F, 0x23, 0) or die("Couldn't locate byte with offset 0x23 of $_[0]: $!");
    print F chr(0);
    close(F) or die("Couldn't close the $_[0] file: $!");
}

# Clean VIDEO_TS.IFO and then its backup (VIDEO_TS.BUP).
clean_file("VIDEO_TS.IFO");
clean_file("VIDEO_TS.BUP");

exit 0;
