>From 4d4dbfe30a047f2d34a74f961829c3398e617894 Mon Sep 17 00:00:00 2001
From: Wouter Verhelst <wouter@debian.org>
Date: Sun, 10 Jan 2016 19:37:20 +0100
Bug-Debian: https://bugs.debian.org/810641
Subject: [PATCH 2/4] Implement binary search for skipping over bad data

Currently, dvdbackup has three options:
- Abort when encountering errors. This is not helpful.
- When encountering errors, skip over whatever we were trying to read
  and move on to the next part. If the amount of unreadable data (in
  this part of the DVD) is small, and we were at the beginning of the
  buffer, this will throw away a lot of data.
- When encountering errors, assume it's just the current block that
  fails and try to read the next. Since most DVD readers will already
  retry reading blocks several times, which can take minutes with some
  hardware, this mode takes forever if there are a large amount of
  unreadable blocks.

This patch adds a binary search mode to reduce both the number of blocks
read as well as the amount of data lost.

It may still lose data if the buffer we're trying to read has two
contiguous areas of unreadable data. I've decided not to care about
that.
---
 src/dvdbackup.c | 74 ++++++++++++++++++++++++++++++++++++++++++++++++++++++---
 src/dvdbackup.h |  3 ++-
 2 files changed, 73 insertions(+), 4 deletions(-)

--- a/src/dvdbackup.c
+++ b/src/dvdbackup.c
@@ -41,6 +41,8 @@
 #include <dvdread/dvd_reader.h>
 #include <dvdread/ifo_read.h>
 
+#include <errno.h>
+
 
 #define MAXNAME 256
 
@@ -834,6 +836,9 @@ static int DVDCopyBlocks(dvd_file_t* dvd
 	float totalMiB = (float)(total) / 512.0f; // total size in [MiB]
 	int to_read = BUFFER_SIZE;
 	int act_read; /* number of buffers actually read */
+	int binary_last_bad = 0;
+	int binary_last_good = 0;
+	int binary_orig_start = 0;
 
 	/* Write buffer */
 	unsigned char buffer[BUFFER_SIZE * DVD_VIDEO_LB_LEN];
@@ -902,17 +907,80 @@ static int DVDCopyBlocks(dvd_file_t* dvd
 				numBlanks = to_read - act_read;
 				fprintf(stderr, _("padding %d blocks\n"), numBlanks);
 				break;
+
+			case STRATEGY_SEARCH_BINARY:
+				/* There are three possibilities of us
+				 * getting here:
+				 * - We're here the first time. In that
+				 *   case, we should try to read the last
+				 *   block of to_read to see if any part
+				 *   of the blocks we were trying to
+				 *   read can actually be read and start
+				 *   the binary search.
+				 * - We're here the second time. In that
+				 *   case, we assume they're all
+				 *   unreadable and skip to the next
+				 *   part.
+				 * - In all other cases, we verify if
+				 *   the just-failed block is one below
+				 *   the most recent successful block.
+				 *   If so, we skip ahead to the end of the
+				 *   part. If not, we skip to a point halfway
+				 *   between the last successful point and this
+				 *   point and try again.
+				 */
+				if(binary_last_bad == 0) {
+					// first time
+					binary_last_bad = binary_orig_start = offset;
+					numBlanks = to_read - act_read - 1;
+					to_read = 1;
+					fprintf(stderr, _("trying %d blocks ahead\n"), numBlanks);
+				} else if(binary_last_good < binary_last_bad) {
+					// second time
+					fprintf(stderr, _("moving on, padding %d blocks\n"), offset - binary_last_bad);
+					binary_last_bad = 0;
+					to_read = BUFFER_SIZE;
+				} else {
+					binary_last_bad = offset;
+					if(binary_last_bad + 1 == binary_last_good) {
+						fprintf(stderr, _("first readable block is %d, moving on (total skipped: %d)\n"), offset + 1, offset + 1 - binary_orig_start);
+						binary_last_bad = binary_last_good = 0;
+						to_read = BUFFER_SIZE;
+						numBlanks = 1;
+					} else {
+						numBlanks = (binary_last_good - offset) / 2;
+						fprintf(stderr, _("trying %d blocks ahead (gap size: %d)\n"), numBlanks, binary_last_good - offset);
+					}
+				}
 			}
 
 			/* pretend we read what we padded */
 			offset += numBlanks;
 			remaining -= numBlanks;
 
-			if (lseek(destination, (off_t)(offset * DVD_VIDEO_LB_LEN), SEEK_SET)) {
-				fprintf(stderr, _("Error seeking %s\n"), filename);
+			if (lseek(destination, (off_t)offset * DVD_VIDEO_LB_LEN, SEEK_SET) < 0) {
+				fprintf(stderr, _("Error seeking %s: %s\n"), filename, strerror(errno));
 				return 1;
 			}
-
+		} else {
+			if(errorstrat == STRATEGY_SEARCH_BINARY && binary_last_bad != 0) {
+				offset -= act_read;
+				binary_last_good = offset;
+				if(binary_last_bad + 1 == binary_last_good) {
+					fprintf(stderr, _("first readable block is %d, moving on (total skipped: %d)\n"), offset, offset - binary_orig_start);
+					binary_last_bad = binary_last_good = 0;
+					to_read = BUFFER_SIZE;
+				} else {
+					int search = (binary_last_good - binary_last_bad) / 2;
+					offset -= search;
+					remaining += search;
+					fprintf(stderr, _("success, trying %d blocks back (gap size: %d)\n"), search, binary_last_good - binary_last_bad);
+					if(lseek(destination, (off_t)offset * DVD_VIDEO_LB_LEN, SEEK_SET) < 0) {
+						fprintf(stderr, _("Error seeking %s: %s\n"), filename, strerror(errno));
+						return 1;
+					}
+				}
+			}
 		}
 
 		if(progress) {
--- a/src/dvdbackup.h
+++ b/src/dvdbackup.h
@@ -32,7 +32,8 @@ extern int progress;
 typedef enum {
 	STRATEGY_ABORT,
 	STRATEGY_SKIP_BLOCK,
-	STRATEGY_SKIP_MULTIBLOCK
+	STRATEGY_SKIP_MULTIBLOCK,
+	STRATEGY_SEARCH_BINARY,
 } read_error_strategy_t;
 
 int DVDDisplayInfo(dvd_reader_t*, char*);
